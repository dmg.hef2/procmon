package com.hefesto.procmon

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*
import android.widget.AdapterView.AdapterContextMenuInfo
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    lateinit var adp_pi: MyAdapter
    lateinit var lv_proc: ListView
    lateinit var proc_list : List<ProcessInfo>

    lateinit var tv_temp : TextView

    lateinit var temp : String

    var sort_selector : Int = 0

    private val handler = Handler(Looper.getMainLooper())

    private val refresh_process = object : Runnable {
        override fun run() {
            loadData()
            draw_task()
            handler.postDelayed(this, 5000)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        handler.post(refresh_process)

    }

    override fun onStart() {
        super.onStart()
    }

    protected override fun onPause() {
        super.onPause()
        handler.removeCallbacks(refresh_process)
        finish()
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        getMenuInflater().inflate(R.menu.process_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterContextMenuInfo
        when(item.itemId) {
            R.id.kill -> {
                Toast.makeText(this, "Selected process killed!", Toast.LENGTH_LONG).show()
                return true
            }
            else -> {}
        }
        return super.onContextItemSelected(item)
    }

    // Vincula as variaveis aos IDs dos elementos visuais
    private fun initView()
    {
        tv_temp = findViewById(R.id.tv_temp)
        lv_proc = findViewById(R.id.list)
        registerForContextMenu(lv_proc)
    }

    // Atualiza a lista de processos
    private fun loadData()
    {
        refreshProcessList()
        proc_list = getProcessInfo()
        proc_list = when(sort_selector) {
            0 -> proc_list.sortedByDescending { it.cpu_usage }
            1 -> proc_list.sortedByDescending { it.name }
            2 -> proc_list.sortedByDescending { it.pid }
            else -> proc_list.sortedByDescending { it.cpu_usage }
        }
        temp = getTemperature()
    }

    // Atualiza a ListViewer
    private fun draw_task(){
        tv_temp.setText(temp.subSequence(0, 2).padEnd(3,'º').padEnd(4,'C'))
        adp_pi = MyAdapter(this@MainActivity, R.layout.single_item, proc_list)
        lv_proc.setAdapter(adp_pi)
        lv_proc.invalidateViews()
    }

    /**
     * A native library that implements the process monitoring functions
     */
    external fun getProcessInfo() : List<ProcessInfo>
    external fun refreshProcessList() : Unit
    external fun getTemperature() : String

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native")
        }
    }

}